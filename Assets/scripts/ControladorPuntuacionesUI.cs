﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.SceneManagement;

//Para insertar en BBDD
using Mono.Data.Sqlite;
using System.Data;
//using System;

public class ControladorPuntuacionesUI : MonoBehaviour {

    public Text texto;

    public void VolverMenu()
    {
        SceneManager.LoadScene("sceneMenu");
    }



    // Use this for initialization
    void Start () {

        string conn = "URI=file:" + Application.dataPath + "/bbdd2.db"; //Path to database.
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(conn);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = "SELECT * FROM puntos";
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();

        string nombre = "";
        string puntos = "";

        while (reader.Read())
        {
            //int id = reader.GetInt32(0);
            nombre = reader.GetString(1);
            puntos = reader.GetString(2);

            texto.text+="Nombre =" + nombre + "  puntos =" + puntos;
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
