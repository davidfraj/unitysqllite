﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Para llamar a los elementos del UI
using UnityEngine.UI;

public class CanvasCargar : MonoBehaviour {

    private int contador;
    private string texto;

    private void Awake()
    {
        contador = 0;
        texto = "";
    }

    // Use this for initialization
    void Start () {
        //Esto si que funciona:
        print(gameObject.GetComponentInChildren<Text>().text);
        print(gameObject.GetComponentInChildren<Slider>().value);
    }
	
	// Update is called once per frame
	void Update () {
        contador++;
        texto = contador.ToString();
        gameObject.GetComponentInChildren<Text>().text = texto;
	}
}
