﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventosUI : MonoBehaviour {

    public string edad;
    public Text miTextoChungo;
    public InputField miTextoEntrada;

    //El evento debe ser publico para que se pueda asociar a un evento click del boton
    public void PulsoBoton()
    {
        print("has pulsado un boton");
    }

    public void CambiaTexto()
    {
        miTextoChungo.text = miTextoEntrada.text;
    }

    public void PulsaElBoton(string numero)
    {
        print(numero);
    }

}
