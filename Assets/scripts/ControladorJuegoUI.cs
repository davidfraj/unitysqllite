﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

//Para insertar en BBDD
using Mono.Data.Sqlite;
using System.Data;
//using System;

public class ControladorJuegoUI : MonoBehaviour {

    public Text texto;
    public Text textoOperacion;
    public Text textoAciertos;
    public Text textoErrores;
    public Text textoCronometro;
    private int resultado;
    private int aciertos;
    private int errores;
    private float tiempo;

    private void Awake()
    {
        //Empiezo el texto a completamente vacio
        texto.text = "";
        textoOperacion.text = "";
        aciertos = 0;
        errores = 0;
        resultado = 0;
        tiempo = 10f;

        //Realizo la primera pregunta
        nuevaPregunta();
    }

    private void actualizarResultados()
    {
        //Escribo en pantalla los resultados
        textoAciertos.text = "Aciertos: " + aciertos;
        textoErrores.text = "Errores: " + errores;
    }

    private void nuevaPregunta()
    {
        //Actualizo los resultados del marcador
        actualizarResultados();

        //Al realizar la nueva pregunta, escribo en su sitio
        int numero1 = UnityEngine.Random.Range(1, 20);
        int numero2 = UnityEngine.Random.Range(1, 20);
        resultado = numero1 + numero2;
        textoOperacion.text = numero1 + " + " + numero2;
    }

    public void PulsarBoton(string valor)
    {
        if (valor != "INTRO")
        {
            texto.text += valor;
        }
        else
        {

            //Evaluo la respuesta
            //Actualizo el marcador
            //if (resultado==Convert.ToInt32(texto.text))
            if (resultado.ToString() == texto.text)
            {
                aciertos++;
            }
            else
            {
                errores++;
            }

            //Calculo respuesta y vacio texto
            texto.text = "";

            print("Res1:" + resultado.ToString());
            print("Res2:" + texto.text);

            //Vuelvo a preguntar
            nuevaPregunta();
        }

        
    }

    private void Update()
    {
        //Resto un tiempo
        tiempo -= Time.deltaTime;

        //Evaluo el cronometro
        textoCronometro.text = tiempo.ToString("00");

        if (tiempo < 0)
        {

            //GUARDO LA PUNTUACION EN LA BBDD
            string conn = "URI=file:" + Application.dataPath + "/bbdd2.db"; //Path to database.
            IDbConnection dbconn;
            dbconn = (IDbConnection)new SqliteConnection(conn);
            dbconn.Open(); //Open connection to the database.
            IDbCommand dbcmd = dbconn.CreateCommand();
            string sqlQuery = "INSERT INTO puntos(nombre, puntos)VALUES('David',' "+aciertos.ToString()+"')";
            dbcmd.CommandText = sqlQuery;
            dbcmd.ExecuteNonQuery();
            
            dbcmd.Dispose();
            dbcmd = null;
            dbconn.Close();

            //CAMBIO DE ESCENA
            SceneManager.LoadScene("sceneMenu");
        }
    }
}
